import datetime
import base64, hmac, hashlib
import json
import os
from download import *
import requests
from credentials import API_KEY  # Login info now stored in credentials.py
from credentials import API_SECRET  # Login info now stored in credentials.py

HOST = 'api.magisto.com'


def canonical_string(url, host, timestamp):
    return "{}:{}:{}".format(url, host, timestamp)


def sign_request(api_secret, url, host, timestamp):
    canonical = canonical_string(url, host, timestamp)
    sig_hmac = hmac.new(api_secret, canonical, digestmod=hashlib.sha256)
    b64_hmac = base64.encodestring(sig_hmac.digest()).strip()
    return b64_hmac


def request(path, extra_data=None, protocol='http' , files=None):

    if not extra_data or not isinstance(extra_data, dict):
        extra_data = {}
        
    timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"
    api_signature = sign_request(API_SECRET, path, HOST, timestamp)
    
    data = {'api_key': API_KEY, 
            'api_signature': api_signature, 
            'api_timestamp': timestamp}
            
    data.update(extra_data)      
    response = requests.post('{}://{}{}'.format(protocol, HOST, path), data=data, files=files)
    
    return json.loads(response.text)


class video:
    def __init__(self, name, mime, postname):
        self.name = name
        self.mime = mime
        self.postname = postname


def create_simple_video(vsid,video_title, theme_id, audio_track_id,sources):
  path = '/v2/video/create'
    
  extra_data = {
  	  'vsid': vsid,	
      'video_title': video_title, 
      'theme_id': theme_id,
      'audio_track_id': audio_track_id, 
      'sources': sources
  }
  
  response = request(path,extra_data)
  
  return response

def start_session():
    path = '/v2/video/upload/start'
    response =  request(path)
    return response['vsid']

def set_meta(vsid, title, theme_id, audio_track_id, quality):
    path = '/v2/video/upload/meta';
    
    extra_data = {  
        "vsid": vsid,
        "theme_id": theme_id,
        "video_title" : title,
        "audio_track_id": audio_track_id, 
        "duration": 15,
    	"quality": quality
    }
    
    
    response = request(path,extra_data);
    
    return response;


def upload_video(vsid, url,order): 
    path = '/v2/video/upload/video'
    extra_data = {    
        "vsid": vsid,
        'url': url,
        'order': order,
        'caption': '',
    }

    response = request(path,extra_data)
    
    return response


def upload_soundtrack(vsid, url): 
    path = '/v2/video/upload/soundtrack'
    extra_data = {    
        "vsid": vsid,
        'url': url,
    }

    response = request(path,extra_data)
    
    return response 
    
def upload_video_file(vsid, mfile, order,protocol='http'): 
	path = '/v2/video/upload/video'
	mbase = os.path.basename(mfile)
	
	
	timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"
	api_signature = sign_request(API_SECRET, path, HOST, timestamp)
    
	files = {'file': (mbase , open(mfile, 'rb'), 'video/mp4')}

	data = {'api_key': API_KEY, 
			'api_signature': api_signature, 
			'api_timestamp': timestamp,
			'vsid': vsid,
			}
	
	response = requests.post('{}://{}{}'.format(protocol, HOST, path), data=data, files=files)
    
	return response   
	
def upload_sound_file(vsid, mfile, order = None,protocol='http'): 
	path = '/v2/video/upload/soundtrack'
	mbase = os.path.basename(mfile)
	
	
	timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"
	api_signature = sign_request(API_SECRET, path, HOST, timestamp)
    
	files = {'file': (mbase , open(mfile, 'rb'), 'audio/mpeg')}

	data = {'api_key': API_KEY, 
			'api_signature': api_signature, 
			'api_timestamp': timestamp,
			'vsid': vsid,
			}
	
	response = requests.post('{}://{}{}'.format(protocol, HOST, path), data=data, files=files)
    
	return response   	 
    
def end_session(vsid):
    path = '/v2/video/upload/ready'

    extra_data = {    
        "vsid": vsid,
    }

    response = request(path,extra_data)
    
    return response

def status(vsid):
    path = '/v2/video/check'

    extra_data = {    
        "vsid": vsid,
    }

    response = request(path,extra_data)
    
    return response        

vsid = start_session()

print vsid
print set_meta(vsid,"", 112, 22744, "hd")


# Read txt file with data
f = open('data.json', 'r')
x = f.readlines()
print x

counter = 1

for v in x:
	if v.split(".")[-1] == "mp3":
		v = v.replace("\n","")
		# mp3's dont work yet: {u'status': u'FAIL', u'errcode': 7202, u'error': u'invalid mime type - audio/mpeg (6edcf90a-7f12-40be-b746-20a897f7b155.mp3)'}
		print upload_sound_file(vsid, v)
	else:
		v = v.replace("\n","")
		#print upload_video(vsid, v, counter)
		print upload_video_file(vsid, v, counter)
		counter += 1

print end_session(vsid)
#print status(vsid)

with open('vsid.json', 'w') as outfile:
	json.dump(vsid, outfile)

downloadit()	

# Doesn't work:
#sources = [{'url': 'URL1', 'order': 1}, {'url': 'URL2', 'order': 2, 'caption': '1', 'caption_size':'s'}]
#print create_simple_video(vsid,"First try", 112, 22744, sources)