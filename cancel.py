import datetime
import base64, hmac, hashlib
import json
import os
import requests
import urllib
from credentials import API_KEY  # Login info now stored in credentials.py
from credentials import API_SECRET  # Login info now stored in credentials.py

HOST = 'api.magisto.com'


def canonical_string(url, host, timestamp):
    return "{}:{}:{}".format(url, host, timestamp)


def sign_request(api_secret, url, host, timestamp):
    canonical = canonical_string(url, host, timestamp)
    sig_hmac = hmac.new(api_secret, canonical, digestmod=hashlib.sha256)
    b64_hmac = base64.encodestring(sig_hmac.digest()).strip()
    return b64_hmac


def request(path, extra_data=None, protocol='http'):

    if not extra_data or not isinstance(extra_data, dict):
        extra_data = {}
        
    timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"
    api_signature = sign_request(API_SECRET, path, HOST, timestamp)
    
    data = {'api_key': API_KEY, 
            'api_signature': api_signature, 
            'api_timestamp': timestamp}
            
    data.update(extra_data)      
    response = requests.post('{}://{}{}'.format(protocol, HOST, path), data=data)
    
    return json.loads(response.text)


def cancel(vsid):
    path = '/v2/video/upload/cancel'

    extra_data = {    
        "vsid": vsid,
    }

    response = request(path,extra_data)
    
    return response        

data = json.load(open('vsid.json'))
print cancel(data)


